# 基于JavaWeb的橙园社区管理系统

#### 介绍
基于JavaWeb的依托于成都大学橙园社区的管理系统

#### 软件架构
软件架构说明<br>
MVC架构<br>
jdk:java11<br>
服务器:Tomcat 11<br>
数据库:Mysql 8.0<br>
视图层采用Vue3+Element-Plus<br>
控制层采用Servlet<br>

#### 安装教程

1.  git clone + 项目地址
2.  将/resource 添加为 resource root, 将 /resource/jdbc.properties <br>中 password修改为自己的数据库root用户的密码
3.  将lib下的所有jar包 add as library
4.  添加tomcat 11服务器, Application context 设置为/
5.  运行sql脚本，创建数据库
6.  删除out目录
7.  配置前端项目 打开/fronted/ChengziCommunity npm install 

#### 使用说明

1.  run tomcat 启动后端服务器
2.  打开fronted, npm run dev, 启动前端服务

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5.  本项目贡献者ChrisHong Liu Tang


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
